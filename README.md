# Usage:
  * Add the following to the *parent's* package dependencies: (alternative would be using peerDependencies but.. no!)

        "zs-statics": "git+ssh://git@gitlab.zoomsquare.com:packages/zs-statics.git"
        "zs-logging": "git+ssh://git@gitlab.zoomsquare.com:packages/zs-logging.git"

  ```
    schemas = require("zs-schemas")
    ...
    RealEstate = schemas.model("RealEstate","live"/"test"*)
    OR
    mongoose.connect (..)
    RealEstateSchema = schemas.RealEstate
    RealEstate = mongoose.model "RealEstate", RealEstateSchema
  ```

  or just omit the second parameter and specify env `ZS_SCHEMAS_LANE=live/test` to set for all.



# used by:

* pipeline
* crawler
* realestate-service
* zionic
* webmastertools
* xtr-oraculum
* push-service
* trefferupdate
* seo-generator
* zs-admin
* privates top-secret andy-projekt
