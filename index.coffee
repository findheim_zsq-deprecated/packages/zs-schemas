_ = require "lodash"

module.exports =
  AnnotEstate:      require './lib/annotestate'
  Content:          require './lib/content'
  GeoInfo:          require './lib/geoinfo'
  Immohost:         require './lib/immohost'
  OverwriteEstate:  require './lib/overwriteestate'
  PipelineState:    require './lib/pipelinestate'
  RealEstate:       require './lib/realestate'
  SearchProfile:    require './lib/searchprofile'
  User:             require './lib/user'
  AdminUser:        require './lib/adminuser'
  AdminRole:        require './lib/adminrole'
  Roles:            require './lib/roles'
  Webmaster:        require './lib/webmaster'
  XTRannotEstate:   require './lib/xtrannotestate'
  FbAdCampaign:     require './lib/fbAdCampaign'
  FbAdSet:          require './lib/fbAdSet'
  FbAdLeadForm:     require './lib/fbAdLeadForm'
  FbAd:             require './lib/fbAd'
  FbAdLead:         require './lib/fbAdLead'
  FbAdCreative:     require './lib/fbAdCreative'
  FbQueue:          require './lib/fbQueue'
  fbPush:           require './lib/fbPush'
  FbConfig:         require './lib/fbConfig'
  FbPost:           require './lib/fbPost'
  FbPostV2:         require './lib/fbPostV2'
  FbGroups:         require './lib/fbGroups'
  FbPages:          require './lib/fbPages'
  FbUsers:          require './lib/fbUsers'
  PremiumEstate:    require './lib/premiumEstate'
  Email:            require './lib/email'
  EmailBlacklist:   require './lib/emailBlacklist'
  EmailOpenimmoBlacklist:   require './lib/emailOpenimmoBlacklist'
  EmailWhitelist:   require './lib/emailWhitelist'
  EmailBcc:   require './lib/emailBcc'
  Leads:            require './lib/leads'
  MfStrings:        require './lib/mfStrings'
  Seo:              require './lib/seo'
  Statistics:       require './lib/statistics'
  Translations:     require './lib/translations'
  TranslationsV2:     require './lib/translationsV2'
  Neighborhood:     require './lib/neighborhood'
  Features:         require './lib/features'
  seoLandingGroups: require './lib/seoLandingGroups'
  seoLandingProfiles: require './lib/seoLandingProfiles'
  webTopicPage:    require './lib/webTopicPage'
  webDevelopmentProject:  require './lib/webDevelopmentProject'
  webPropertyDeveloper:    require './lib/webPropertyDeveloper'
  webFiles:  require './lib/webFiles'
  apifyCrawlingHistory: require './lib/apifyCrawlingHistory'
  mongo:            require './lib/_mongo'
  lanes:
    'AnnotEstate':
      live: "backend"
      test: "test"
    "Content":
      live: "live"
      test: "next"
    "GeoInfo":
      live: "live"
      test: "next"
    'Immohost':
      live: "backend"
      test: "test"
    'OverwriteEstate':
      live: "backend"
      test: "test"
    'PipelineState':
      live: "backend"
      test: "test"
    'RealEstate':
      live: "backend"
      test: "test"
    'SearchProfile':
      live: "live"
      test: "next"
    'User':
      live: "live"
      test: "next"
    'AdminUser':
      live: "live"
      test: "next"
    'AdminRole':
      live: "live"
      test: "next"
    'Roles':
      live: "live"
      test: "next"
    'Webmaster':
      live: "backend"
      test: "test"
    'XTRannotEstate':
      live: "backend"
      test: "test"
    'FbAdCampaign':
      live: "live"
      test: "next"
    'FbAdSet':
      live: "live"
      test: "next"
    'FbAdLeadForm':
      live: "live"
      test: "next"
    'FbAd':
      live: "live"
      test: "next"
    'FbAdLead':
      live: "live"
      test: "next"
    'FbAdCreative':
      live: "live"
      test: "next"
    'FbQueue':
      live: "live"
      test: "next"
    'fbPush':
      live: "live"
      test: "next"
    'FbConfig':
      live: "live"
      test: "next"
    'FbPost':
      live: "live"
      test: "next"
    'FbPostV2':
      live: "live"
      test: "next"
    'FbGroups':
      live: "live"
      test: "next"
    'PremiumEstate':
      live: "live"
      test: "next"
    'FbPages':
      live: "live"
      test: "next"
    'FbUsers':
      live: "live"
      test: "next"
    'Email':
      live: "live"
      test: "next"
    'EmailBlacklist':
      live: "live"
      test: "next"
    'EmailOpenimmoBlacklist':
      live: "live"
      test: "next"
    'EmailWhitelist':
      live: "live"
      test: "next"
    'EmailBcc':
      live: "live"
      test: "next"
    'Leads':
      live: "live"
      test: "next"
    'MfStrings':
      live: "live"
      test: "next"
    'Seo':
      live: "live"
      test: "next"
    'Statistics':
      live: "live"
      test: "next"
    'Translations':
      live: "live"
      test: "next"
    'TranslationsV2':
      live: "live"
      test: "next"
    'Neighborhood':
      live: "live"
      test: "next"
    'Features':
      live: "live"
      test: "next"
    'seoLandingGroups':
      live: "live"
      test: "next"
    'seoLandingProfiles':
      live: "live"
      test: "next"
    'webTopicPage':
      live: "live"
      test: "next"
    'webPropertyDeveloper':
      live: "live"
      test: "next"
    'webDevelopmentProject':
      live: "live"
      test: "next"
    'webFiles':
      live: "live"
      test: "next"
    'apifyCrawlingHistory':
      live: "live"
      test: "next"

  setDefaultLane: (laneOrConnstr) ->
    console.log "ZS-SCHEMAS: Setting default lane to '#{laneOrConnstr}'"
    @defaultlane = laneOrConnstr

  setDefaultLaneToTest: (to = true) ->
    @setDefaultLane(if to then "test" else "live")

  _getConnStrFor: (laneOrConnstr, name) ->
    unless laneOrConnstr?
      laneOrConnstr = @defaultlane

    connStr = ""
    usingLane = laneOrConnstr in ["live","test"]
    #    if table?
    #      console.log @[name]
    if usingLane
      unless name in _.keys(@lanes)
        throw new Error("Unknown schema name #{name}")
      connStr = @lanes[name][laneOrConnstr]
    else
      connStr = laneOrConnstr

    connStr

  model: (name, laneOrConnstr, mongoOptions = {}) ->
    unless laneOrConnstr?
      laneOrConnstr = @defaultlane
    connStr = @_getConnStrFor laneOrConnstr, name
    if connStr is laneOrConnstr
      console.log "ZS-SCHEMAS: Connecting to #{name} on mongo #{connStr}"
    else
      console.log "ZS-SCHEMAS: Connecting to #{name} on mongo #{connStr}, which is lane #{laneOrConnstr}"

    @mongo.conn(connStr, mongoOptions).model name, @[name]

  disconnect: ()->
    for key, conn of @mongo.getConnections()
      conn.close()
    @mongo.mongoose.disconnect()

if process.env.ZS_SCHEMAS_LANE?
  console.log "ZS-SCHEMAS: found ZS_SCHEMAS_LANE=#{process.env.ZS_SCHEMAS_LANE}."
  module.exports.setDefaultLane process.env.ZS_SCHEMAS_LANE
else if process.env.ZS_ENV is "production"
  console.log "ZS-SCHEMAS: found ZS_ENV=#{process.env.ZS_ENV}."
  module.exports.setDefaultLane "live"
else if process.env.NODE_ENV is "production"
  console.log "ZS-SCHEMAS: found NODE_ENV=#{process.env.NODE_ENV}."
  module.exports.setDefaultLane "live"
else
  console.log "ZS-SCHEMAS: no relevant env set, setting default lane to 'test'"
  module.exports.setDefaultLane "test"