mongoose = require "mongoose"
require('mongoose-regexp')(mongoose)

try
  statics = require "zs-statics"
catch e
  prequire = require 'parent-require'
  statics = prequire "zs-statics"

unless statics?
  statics = require "../../zs-statics"

connections = {}

module.exports =
  debug: (to) ->
    mongoose.set('debug', to)

  conn: (laneOrConnstring, mongoOptions = {}) ->
    
    unless mongoOptions.server?
      mongoOptions.server = {}
    unless mongoOptions.server.socketOptions?
      mongoOptions.server.socketOptions = {}
    unless mongoOptions.server.socketOptions.keepAlive?
      mongoOptions.server.socketOptions.keepAlive = 1

    unless mongoOptions.replSet
      mongoOptions.replSet = {}
    unless mongoOptions.replSet.socketOptions?
      mongoOptions.replSet.socketOptions = {}
    unless mongoOptions.replSet.socketOptions.keepAlive?
      mongoOptions.replSet.socketOptions.keepAlive = 1

    unless mongoOptions.replset
      mongoOptions.replset = {}
    unless mongoOptions.replset.socketOptions?
      mongoOptions.replset.socketOptions = {}
    unless mongoOptions.replset.socketOptions.keepAlive?
      mongoOptions.replset.socketOptions.keepAlive = 1

    if laneOrConnstring is "backend"
      unless mongoOptions.replSet.socketOptions.socketTimeoutMS?
        mongoOptions.replSet.socketOptions.socketTimeoutMS = 60000

    usingLane = laneOrConnstring in Object.keys(statics.mongo)
    if usingLane
      connString = statics.getDBString("mongo",laneOrConnstring)
    else
      connString = laneOrConnstring

    unless connections[connString]?
      # http://mongoosejs.com/docs/api.html#index_Mongoose-createConnection
      
      conn = mongoose.createConnection connString, mongoOptions

      connections[connString] = conn
      if mongoose.get 'debug'

        logger = require("parent-require")("zs-logging").getLogger()
        logger.notice "created mongo connection", {connectionString: connString}

        conn.on 'open', () ->
          console.log "Connection to MongoDB #{laneOrConnstring} established."
        conn.on 'connected', () ->
          console.log "Connected to MongoDB #{laneOrConnstring}."
        conn.on 'close', () ->
          console.log "Closing connection to MongoDB #{laneOrConnstring}."
        conn.on 'error', (err) ->
          console.error "Connection to MongoDB #{laneOrConnstring} failed: #{err.message or err}"
        conn.on 'timeout', (err) ->
          console.error "Connection to MongoDB #{laneOrConnstring} timed out"
        conn.on 'disconnected', (err) ->
          console.error "Connection to MongoDB #{laneOrConnstring} disconnected"
        conn.on 'parseError', (err) ->
          console.error "Connection to MongoDB #{laneOrConnstring} parse Error: #{err.message or err}"
    else
      conn = connections[connString]

    conn
    
  getConnections: ()->
    return connections

  Schema: mongoose.Schema
  mongoose: mongoose