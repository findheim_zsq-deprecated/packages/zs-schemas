{conn, Schema} = require './_mongo'
SALT_WORK_FACTOR = 10

AdminRole = Schema
  name:
    type: String
    required: true
    index:
      unique: true
  active: Boolean
  apiPattern: [
    url: String
    method: [String]
  ]
  statePattern: [String]

module.exports = AdminRole