{conn, Schema} = require './_mongo'

AdminUser = Schema
  name:
    type: String
    required: true
    index:
      unique: true
  password:
    type: String
    required: true
  active: Boolean
  roles: [String]
  profile:
    fbTrackId: String

module.exports = AdminUser