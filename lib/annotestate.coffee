{conn, Schema} = require './_mongo'

# added by AndyL - stores annotations (as part of HTML, generated based on fragments) and props for Golden Standard Corpus
AnnotEstate = Schema
  comment: String
  checked: Boolean
  onlyType: Boolean
  config: Schema.Types.Mixed
  extId: String
  fqdn: String
  html: String
  props: Schema.Types.Mixed
  domain: String
  url: String
  # TODO: Don't forget to add annoteset (Since we now have annotations from IS24 and in the future will add more from other sites)

AnnotEstate.index {url: 1}, {unique: true, dropDups: true}
AnnotEstate.index {domain: 1}
AnnotEstate.index {extId: 1}, {unique: true, dropDups: true}

module.exports = AnnotEstate