{conn, Schema} = require './_mongo'

apifyCrawlingHistory = Schema
  apifyId: String
  developerId: Schema.Types.ObjectId
  meta: Schema.Types.Mixed
  createdAt: Date

apifyCrawlingHistory.index {_id: 1}, {unique: true, dropDups: true}

module.exports = apifyCrawlingHistory