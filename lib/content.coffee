{conn, Schema} = require './_mongo'

# added by AndyL - stores annotations (as part of HTML, generated based on fragments) and props for Golden Standard Corpus
Content = Schema
  _id: String
,
    strict:false
    collection: "content"

module.exports = Content