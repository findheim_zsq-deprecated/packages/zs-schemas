{conn, Schema} = require './_mongo'

email = Schema
  createdAt: Date
  country: String
  id: String
  amazonId: String
  template: String
  emailAddress: String
  bcc: Boolean
  originalId: String
  originalAddress: String
  bccId: Schema.Types.ObjectId
  userId: String
  personId: String
  whitelistId: Schema.Types.ObjectId
  trackingLabel: String
  notifications: [String]
  opened: Boolean
  clicked: Boolean

email.index {id: 1}, {unique: true, dropDups: true}
email.index {amazonId: 1}, {unique: true, dropDups: true}
email.index {template: 1}
email.index {date: 1}
email.index {opened: 1}
email.index {clicked: 1}

module.exports = email