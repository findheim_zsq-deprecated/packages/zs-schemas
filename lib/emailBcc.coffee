{conn, Schema} = require './_mongo'

emailBcc = Schema
  address: String
  active: Boolean
  inactiveReason: String
, {collection: 'emailsBcc'}

emailBcc.index {address: 1}, {unique: true, dropDups: true}
emailBcc.index {active: 1}

module.exports = emailBcc