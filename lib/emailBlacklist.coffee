{conn, Schema} = require './_mongo'

emailBlacklist = Schema
  address: String
  date: Date
  reason: String
, {collection: 'emailsBlacklist'}

emailBlacklist.index {address: 1}, {unique: true, dropDups: true}

module.exports = emailBlacklist