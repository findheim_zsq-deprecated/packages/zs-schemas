{conn, Schema} = require './_mongo'

emailOpenimmoBlacklist = Schema
  address: String
  reason: String
, {collection: 'emailsOpenimmoBlacklist'}

emailOpenimmoBlacklist.index {address: 1}, {unique: true, dropDups: true}

module.exports = emailOpenimmoBlacklist