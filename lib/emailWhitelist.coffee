{conn, Schema} = require './_mongo'

emailWhitelist = Schema
  address: String
  active: Boolean
  inactiveReason: String
, {collection: 'emailsWhitelist'}

emailWhitelist.index {address: 1}, {unique: true, dropDups: true}
emailWhitelist.index {active: 1}


module.exports = emailWhitelist