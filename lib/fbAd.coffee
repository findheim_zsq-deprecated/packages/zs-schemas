{conn, Schema} = require './_mongo'

fbAd = Schema
  id: String
  name: String
  status: String
  effective_status: String
  created_time: Date
  updated_time: Date
  adset_id: String
  campaign_id: String
  creative: Schema.Types.Mixed
  estate:
    modifiedAt: Date
    createdAt: Date
    extId: String

fbAd.index {"estate.extId": 1, id: 1}, {unique: true, dropDups: true}
fbAd.index {"estate.extId": 1}
fbAd.index {status: 1}
fbAd.index {id: 1}
fbAd.index {adset_id: 1}
fbAd.index {campaign_id: 1}

module.exports = fbAd