{conn, Schema} = require './_mongo'

fbAdCampaign = Schema
  id: String
  created_time: Date
  status: String
  name: String
  spend_cap: Number
  buying_type: String
  objective: String
  personId: String

fbAdCampaign.index {id: 1}, {unique: true, dropDups: true}

module.exports = fbAdCampaign