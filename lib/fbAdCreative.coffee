{conn, Schema} = require './_mongo'

fbAdCreative = Schema
  id: String
  name: String
  body: String
  image_url: String
  thumbnail_url: String
  title: String
  object_url: String
  object_story_id: String
  effective_object_story_id: String
  object_story_spec:
    link_data:
      call_to_action:
        type: String
        value:
          lead_gen_form_id: String
      caption: String
      description: String
      image_hash: String
      link: String
      message: String
      child_attachments: Array
    page_id: String
, 
  typeKey: '$type' #change typeKey, because type is used as a field name

fbAdCreative.index {id: 1}, {unique: true, dropDups: true}

module.exports = fbAdCreative