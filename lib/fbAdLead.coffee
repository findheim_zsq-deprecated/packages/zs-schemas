{conn, Schema} = require './_mongo'

fbAdLead = Schema
  id: String
  field_data: Schema.Types.Mixed
  form_id: String
  ad_id: String
  forwarded: Boolean
  created_time: Date
  extId: String
  shouldBeForwarded: Boolean
  userServiceData: Schema.Types.Mixed

fbAdLead.index {id: 1}, {unique: true, dropDups: true}
fbAdLead.index {form_id: 1}
fbAdLead.index {ad_id: 1}
fbAdLead.index {extId: 1}

module.exports = fbAdLead