{conn, Schema} = require './_mongo'

fbAdLeadForm = Schema
  id: String
  name: String
  created_time: Date
  locale: String
  questions: [
    key: String
    label: String
    type: String
  ]
  follow_up_action_url: String
  legal_content_id: String
  context_card: 
    id: String
    title: String
    style: String
    content: [String]
    button_text: String
  privacy_policy: 
    url: String
  cover_photo: String
  estate:
    extId: String
  status: String
,
  typeKey: '$type' #change typeKey, because type is used as a field name

fbAdLeadForm.index {id: 1}, {unique: true, dropDups: true}

module.exports = fbAdLeadForm