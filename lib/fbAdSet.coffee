{conn, Schema} = require './_mongo'

fbAdSet = Schema
  id: String
  name: String
  created_time: Date
  optimization_goal: String
  billing_event: String
  bid_amount: Number
  is_autobid: Boolean
  daily_budget: Number
  promoted_object: 
    page_id: String
    custom_event_type: String
    pixel_id: String
  targeting: Schema.Types.Mixed
  campaign_id: String
  status: String

fbAdSet.index {id: 1}, {unique: true, dropDups: true}
fbAdSet.index {campaign_id: 1}

module.exports = fbAdSet