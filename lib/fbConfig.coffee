{conn, Schema} = require './_mongo'

fbConfig = Schema
  key: String
  type: String
  value: Schema.Types.Mixed
, {collection: 'fbconfig'}

fbConfig.index {key: 1}, {unique: true, dropDups: true}

module.exports = fbConfig