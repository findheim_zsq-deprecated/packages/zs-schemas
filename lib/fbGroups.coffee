{conn, Schema} = require './_mongo'

fbGroups = Schema
  createdAt: Date
  fbId: String
  name : String
  desc : String
  active: Boolean
  interval: Number
  workingHours: String
  typeText:
    Apartment:
      rented: String
      owned: String
    House:
      rented: String
      owned: String
    FlatShare:
      rented: String
  optionText: Object
  country: String
  sort: Number
  query: [
    _id: false
    geoEntities: [String]
    type: [String]
    rentalFee: [Number]
    purchasePrice: [Number]
    livingSpace: [Number]
    rooms: [Number]
    features_filter: Object
  ]
,
  typeKey: '$type' #change typeKey, because type is used as a field name

fbGroups.index {fbId: 1}, {unique: true, dropDups: true}
fbGroups.index {name: 1}, {unique: true, dropDups: true}
fbGroups.index {country: 1}
fbGroups.index {active: 1}

module.exports = fbGroups