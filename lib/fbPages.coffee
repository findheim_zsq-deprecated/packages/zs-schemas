{conn, Schema} = require './_mongo'

q = require "q"
_ = require "lodash"

fbPages = Schema
  createdAt: Date
  country: String
  active: Boolean
  testing: Boolean
  name: String
  desc: String
  fbId: String
  fbToken: String
  cron: String
  cronAuto: Boolean
  utmSource: String
  texts:
    pre: String
    post: String
    type:
      Apartment:
        rented: String
        owned: String
      House:
        rented: String
        owned: String
      FlatShare:
        rented: String
    features: Object
  query: [
    _id: false
    geoEntities: [String]
    type: [String]
    rentalFee: [Number]
    purchasePrice: [Number]
    livingSpace: [Number]
    rooms: [Number]
    features_filter: Object
  ]
  statistics: Schema.Types.Mixed
  errorLog: [
    date: Date
    status: String
    message: String
  ]
,
  typeKey: '$type' #change typeKey, because type is used as a field name

fbPages.index {fbId: 1}, {unique: true, dropDups: true}
fbPages.index {name: 1}, {unique: true, dropDups: true}
fbPages.index {country: 1}
fbPages.index {active: 1}

## INSTANCE METHODS

fbPages.methods.statCount = (statName, date) ->
  # return digit with leading zero if digit < 10
  pad = (n) -> 
    if n < 10 
      "0#{n}"
    else
      "#{n}"

  unless @statistics?
    @statistics = {}

  # split date
  day   = pad(date.getDate())
  month = pad(date.getMonth() + 1)
  year  = date.getFullYear()

  # initialize object if not available
  @statistics[year] = @statistics[year] || {}
  @statistics[year][month] = @statistics[year][month] || {}
  @statistics[year][month][day] = @statistics[year][month][day] || {}

  unless @statistics[year][month][day][statName]?
    @statistics[year][month][day][statName] = 0

  @statistics[year][month][day][statName]++

  return "statistics.#{year}.#{month}.#{day}"
  
## STATIC METHODS

fbPages.statics.statCount = (pageName, statName, date) ->
  d = q.defer()

  @findOne({"name":pageName}).exec (err, page) ->
    if err?
      d.reject new Error "Failed to get facebook page: #{err.message or err}"
    else
      path = page.statCount statName, date
      page.markModified path # no updating of deep mixed field types
      page.save (err)->
        if err?
          d.reject new Error "Failed to add stat count for page #{page.name}: #{err.message or err}"
        else
          d.resolve()

  d.promise

module.exports = fbPages