{conn, Schema} = require './_mongo'

fbPost = Schema
  createdAt: Date
  postedAt: Date
  status:
    type: String
    enum: ["ready","unused","ignored","posted","unavailable"]
  country:
    type: String
    enum: ["at", "de"]
  entityType:
    type: String
    enum: ["group", "page"]
  fbId: String
  estate:
    createdAt: Date
    extId: String
    isPremium: Boolean
    isFeatured: Boolean
    isOi: Boolean
  log: [
    _id: false
    date: Date
    status: String
    message: String
    dups: [String]
  ]
  scheduled: Date
  overrideText: String

fbPost.index {"estate.extId": 1, fbId: 1}, {unique: true, dropDups: true}
fbPost.index {createdAt: 1}
fbPost.index {postedAt: 1}
fbPost.index {status: 1}
fbPost.index {"estate.createdAt": 1}
fbPost.index {"estate.extId": 1}
fbPost.index {"estate.isPremium": 1}
fbPost.index {"estate.isFeatured": 1}
fbPost.index {"estate.isOi": 1}
fbPost.index {entityType: 1}
fbPost.index {fbId: 1}
fbPost.index {scheduled: 1}
fbPost.index {"log.dups": 1}

module.exports = fbPost