{conn, Schema} = require './_mongo'

fbPost = Schema
  country:
    type: String
    enum: ["at", "de"]
  extId: String
  postLog: [
    _id: false
    date: Date
    fbId: String   
  ]

fbPost.index {extId: 1}, {unique: true, dropDups: true}
fbPost.index {country: 1}
fbPost.index {"postLog.date": 1}
fbPost.index {"postLog.fbId": 1}

module.exports = fbPost