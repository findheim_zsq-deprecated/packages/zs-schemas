{conn, Schema} = require './_mongo'

fbPush = Schema
  createdAt: Date
  processedAt: Date
  status: String
  group: String
  user: Schema.Types.ObjectId
  extId: String
  error: String

fbPush.index {_id: 1}, {unique: true, dropDups: true}
fbPush.index {extId: 1}

module.exports = fbPush