{conn, Schema} = require './_mongo'

fbQueue = Schema
  id: String
  className: String
  fn: String
  attempts: Number
  status: String
  params: Schema.Types.Mixed
  needsVerification: Boolean
  log: [
    date: Date
    status: String
    message: String
  ]
  createdAt: {type: Date, default: Date.now}
  finishedAt: Date

fbQueue.index {id: 1}, {unique: true, dropDups: true}

module.exports = fbQueue