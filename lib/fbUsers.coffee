{conn, Schema} = require './_mongo'

fbUsers = Schema
  createdAt: Date
  name: String
  email: String
  password: String
  birthday: String
  handy: String
  fbId: String
  comment: String
  token: String
  status: String
  error: [Schema.Types.Mixed] #error object from facebook

fbUsers.index {_id: 1}, {unique: true, dropDups: true}

module.exports = fbUsers