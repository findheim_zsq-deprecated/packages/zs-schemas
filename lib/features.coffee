{conn, Schema} = require './_mongo'

Features = Schema
  _id: String         # this is actually Schemas.Types.ObjectId. mongoose .save() wont work unless you set this to ObjectId
  identifier: String
  name: String
  translations:
    en: String
    de: String
  sorting: Number
  active: Boolean
  activeCountries: [String]
  activeApps: [String]
  group: String       # for zionic

module.exports = Features
