{conn, Schema} = require './_mongo'

# added by Velizar Malevski, to migrate the geo information from content collection.
GeoInfo = Schema
  _id: String
  content:
    _id : false
    desc: String
    img: String
    tags: []
  key: String
,
    strict:false
    collection: "geoInfo"

module.exports = GeoInfo