crypto = require 'crypto'
{conn, Schema} = require './_mongo'

url = require("url")

###
 * Config documentation:
 *    regex:
 *      $1 must be an identifier (to sort out doubles caused by session information, etc), so make sure to use ?:,
 *      if you select something else. This regex checks against the PATH of the URL (so '^/(whatever)' matches against
 *      http://host/123 and selects 123). Should the pattern change, because the structure changes, try to create a
 *      regular expression matching both the old and the new format.
 *      $1 must be an identifier (to sort out doubles caused by session information, etc), so make sure to use ?:, if you select something else. This regex checks against the PATH of the URL (so '^/(whatever)' matches against http://host/123 and selects 123). Should the pattern change, because the structure changes, try to create a regular expression matching both the old and the new format.
 *
 *    regexURLFilter:
 *      If this is set to true an additional regex containing real estate related terms must match at URL level. This is
 *      for websites like immo-kleinanzeigen that have various non-estate ads and no special namespace for them. Be
 *      careful as this could result in estates not being detected as such.
 *
 *    check:
 *      A selector that has to be present to make a valid estate. If it is not present it means that the page either
 *      isn't an estate or went offline. Usually something like the "contact real estate agent" buttons work best for
 *      this. id= is preferred for performance reasons.
 *
 *    crawlerId:
 *      Identifies the crawler that is supposed to crawl it.
 *
 *    follow:
 *      This is similar to regex. used to constrict crawling, but overridden by .regex. So for example when you have a
 *      big newspaper you can prevent site to even be considered estates by just checking http://host/real_estates/
 *      specifying '^/real_estates/'. If you want to match all (for small websites and websites with mostly real estate
 *      ads specify '^'.
 *
 *    title:
 *      The title of the real estate (ad). Currently only used for duplicate detection. striphtml is always true.
 *
 *    image:
 *      Select one or multiple elements to extract images from. The src will be extracted, resolved and linked at the
 *      front end. .image.attr is used for extracting the urls. (default: 'src')
 *
 *    fragments:
 *      The real estate's fragment. extractProperty is a "match all". use "strict: true" flag for large content
 *      fragments in order to prevent false positives (will required descriptors such as "Ort: " for addresses)
 *      An exclude property to remove a sub path of the node. This will only select the *FIRST* node selected,
 *      to make it easier to work with complex table in table layouts and layouts with no classes or ids.
 *
 *    preferHTTPS:
 *      Should we _prefer_ HTTPS? This is not actually necessary in most cases, but it prevents redirects.
 *
 *    prioritizeNewEstates:
 *      Usually we do not want to prioritize new real estates in order to be able to keep up with old estates (outdated).
 *      However, if we also want to make sure to index new estates more quickly, we want to prioritze new Estates, This
 *      setting tells the crawler to do so.
 *
 *    state:
 *       awaiting_profile:    Doesn't have a config
 *       awaiting_approval:   Not approved yet
 *       active:              Crawler allowed to crawl
 *       inactive:            Temporarily inactive
 *       testing:             Send to testpipeline only
 *
 * Dates:
 *
 * Users:
 *   - Cralwer
 *   - Oraculum
 *   - Pipeline
 *   - Webmastertools
 *   - Zionic
 *
 * ###


immohostSchema = Schema
  fqdn:       {type: String, unique: true, trim: true}  # Fully Qualified Domain Name
  sitemaps:   [{type: String, trim: true}]              # URLs of Sitemaps
  openimmo:   [{type: String, trim: true}]              # URLs of OpenImmo
  feeds:      [{format: {type: String, trim: true}, url: {type: String, trim: true}, cron: {type: String, trim: true}}] # Feeds
  crawlDelay: Number                                    # Crawl delay in Seconds
  owners:     [String]    # List of verifiedOwners
  state:
    type: String
    enum: ['awaiting_profile', "user_submitted", 'active', "active_set_offline", "active_crawler_issues", 'temporarily_inactive', 'rejected', 'rejected_only_commercial', 'rejected_wrong_country', 'rejected_ads_on_portal', 'rejected_meta_engine', "rejected_currently_no_ads", "rejected_never_any_ads", "crawler_feature", "crawler_feature_frames", "crawler_feature_seeds", "crawler_feature_js", "crawler_feature_iframe","crawler_feature_multiad", "crawler_issue", "xtr_issue", "broken_justimmo", "blocking_us", "duplicate", "under_construction", "not_existing_anymore"]
    default: 'awaiting_profile'
  testpipeline: Boolean
  comment:    String
  issues:     String
  numEstates: Number
  lastEstateFound: Date
  lastOfflineFound: Date
  lastZoomLivePush: Date
  concurrency:  Number
  minTimeToWait:  Number
  preferredCountry:
    type: String
    enum: ["at", "de"]
    default: "at"
  crawlerId:
    type: String
    enum: ['crawler1.zoomsquare.com', 'crawler2.zoomsquare.com', 'pipeline.tp.zoomsquare.com']
    default:'crawler1.zoomsquare.com'
  prioritizeNewEstates: {type: Boolean, default: false}
  config:
    justimmo:     Boolean
    pushDisabled: Boolean
    check:        [String]
    image:        {jpath: String, attr: String}
    preferHTTPS:  Boolean
    regex:        RegExp
    regexURLFilter: Boolean
    follow:       RegExp
    title:        {jpath: String}
    fragments:    [
      jpath: String
      tags: [String]
      strict: Boolean
      striphtml: Boolean
      exclude: String
      replace: [
        regex: String
        replacement: String
      ]
    ]
    collectKnownMicrodata: Boolean
    seeds:        [{method: {type: String, enum: ['POST', 'GET']}, url: String, data: String, contentType: {type:String, enum: ["application/x-www-form-urlencoded","application/json"]}}]
    zionic:       Schema.Types.Mixed
  rights:
    bigImages:    Boolean
    gallery:      Boolean
  _changes:        [{time: { type : Date, default: Date.now }, diff:Schema.Types.Mixed}, comment: String] #https://www.npmjs.com/package/deep-diff

immohostSchema.index {fqdn: 1}, {unique: true, dropDups: true}
immohostSchema.index {"owners": 1}

immohostSchema.statics.addOwner = (fqdn, ownerId, cb) ->
  @findOneAndUpdate {fqdn}, {$addToSet: {owners: new Schema.Types.ObjectId(ownerId)}}, cb

immohostSchema.statics.getExtIdFromUrl = (inurl, cb) ->
  parsed = url.parse inurl
  unless parsed.host?
    cb new Error("Could not find hostname in url '#{inurl}'")
  else
    @findOne {fqdn: parsed.host}, (err, res) ->
      if err?
        cb err
      else if not res?
        cb new Error("Unknown Immohost: #{parsed.host}")
      else if not res.config?.regex?
        cb new Error("Could not find config.regex for #{parsed.host}")
      else
        extId = parsed.path.match(new RegExp(res.config.regex))
        unless extId? and extId[1]
          cb new Error("Could not match path #{parsed.path} to config.regex")
        else
          cb null, parsed.hostname + extId[1]

immohostSchema.methods.ownedBy = (ownerId) ->
  owner = ownerId.toString()
  for id in  @owners
    if id.toString() is owner
      return true
  return false

immohostSchema.statics.justImmorize = (immoHost) ->
  # TODO: http://www.donauimmo.at/immobilien/detail?id=443849&parent=Immobilien
  # TODO: http://www.auer-partner.at/objekte/index.php?action=detail&id=442597&pos=0
  # http://www.donauimmo.at/immobilien/detail?id=443849&parent=Immobilien
  # http://www.donauimmo.at/immobilien/detail/id/443801
  # http://www.hs-immo.at/immobilien/detail/id/202167
  # http://www.auer-partner.at/objekte/index.php?action=detail&id=442597&pos=0
  if immoHost.config?.justimmo
    immoHost.config.regex = /(?:^\/(?:objekt|immobilien)\/detail(?:\/(?:objekt)?id\/|id=)|ji_property_id=)(\d+)$/
    immoHost.config.follow = /^(?!\/mobile|\/expose\/buildExpose).*/
    immoHost.config.check = ['#bg_objectContactForm']
    immoHost.config.crawlDelay = 1

  immoHost

immohostSchema.methods.justImmorize = ->
  immohostSchema.statics.justImmorize @

module.exports = immohostSchema