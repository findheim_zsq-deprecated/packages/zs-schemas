{conn, Schema} = require './_mongo'

leads = Schema
  type:
    $type: String
    enum: ["premium","listing","external"]
  createdAt: Date
  userId: String
  extId: String
  searchQuery:
    type: [String]
    rentalFee: [Number]
    purchasePrice: [Number]
    livingSpace: [Number]
    annualLeaseFee: [Number]
    plotArea: [Number]
    rooms: [Number]
    geoEntities: [String]
    createdAfter: String
    bbox: Schema.Types.Mixed
    geoFields: [String]
    features: Object
    features_filter: Object
    sort: [
      _id: false
      sortBy: String
      order: String
    ]
, {
    collection: 'leads'
    typeKey: '$type'
  }

leads.index {_id: 1}, {unique: true, dropDups: true}
leads.index {userId: 1}

module.exports = leads