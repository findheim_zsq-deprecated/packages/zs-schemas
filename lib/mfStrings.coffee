{conn, Schema} = require './_mongo'

mfStrings = Schema
  key: String
  lang: String
  text: String
  ctime: Number
  mtime: Number
  revisionId: String
  file: String
  line: Number
  template: String
  removed: Boolean
, {collection: 'mfStrings'}

mfStrings.index {_id: 1}, {unique: true, dropDups: true}

module.exports = mfStrings