{conn, Schema} = require './_mongo'

Neighborhood = Schema
  key: String
  data: Schema.Types.Mixed
, {collection: 'neighborhood'}

Neighborhood.index {key: 1}, {unique: true, dropDups: true}

module.exports = Neighborhood