{conn, Schema} = require './_mongo'

#
# An OverwriteEstate is an estate that contains properties
# which will be applied after the extractor ran through it.
# If properties should be removed they should be explicitly
# set to null.
#
# Users:
#   - Pipeline
#


OverwriteEstate = Schema
  extId: String
  forceOffline: Boolean
  props: Schema.Types.Mixed
  update: String # This is a String so we don't interfere with MongoDB

OverwriteEstate.index {extId: 1}, {unique: true, dropDups: true}

module.exports = OverwriteEstate