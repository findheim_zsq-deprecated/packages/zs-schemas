{conn, Schema} = require './_mongo'

#
# This is a KISS format [too many ideas] of pipeline states. They are based on the idea
# that once working aggregation functions will likely never change and if they do the
# base data would be needed (that's why saving RDBs would probably be a good idea). This
# information is actually view bound and optimized for output with HighCharts. This also
# prevents the problem with dots in keys.
#
# Properties:
#   date:     timestamp of creation of this snapshot
#   outputOf: The name of the function
#   pipeline: live or test
#   state:    the actual state Object
#

# TODO: Decide on whether we want to save an RDB file (they are rather small)
PipelineState = Schema
  date: { type: Date, default: Date.now }
  outputOf: String
  state: Schema.Types.Mixed
  pipeline: {type: String, enum: ['live', 'test']}

PipelineState.index {date: 1}
PipelineState.index {pipeline: 1}

module.exports = PipelineState