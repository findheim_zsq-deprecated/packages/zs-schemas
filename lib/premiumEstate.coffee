{conn, Schema} = require './_mongo'

premiumEstate = Schema
  extId: String
  log: [
    date: Date
    status: String
    message: String
    property: String
  ]
  online: Boolean
  isPremium: Boolean
  isFeatured: Boolean
  facebookLeads:
    status: String
    fbAdLead:
      status: String
      id: String
  productNumber: Number
  disabled: Boolean
  leadCount: Number
  createdAt: {type: Date, default: Date.now}

premiumEstate.index {extId: 1}, {unique: true, dropDups: true}

module.exports = premiumEstate