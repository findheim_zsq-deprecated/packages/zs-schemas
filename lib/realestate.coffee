{conn, Schema} = require './_mongo'

RealEstate = Schema {
  address: Schema.Types.Mixed
  extId: String
  fragments: Schema.Types.Mixed
  geo: Schema.Types.Mixed
  meta:
    # Data
    geo: Schema.Types.Mixed
    props: Schema.Types.Mixed
    # Dates
    created: [Date]
    lastCrawled: [Date]
    # Merged Meta Information
    dups: [Schema.Types.Mixed]
    fqdns: [String]
    img: Schema.Types.Mixed
    onlines: [Boolean]
    urls: [String]
    # Sources for props
    sources: Schema.Types.Mixed
  props: Schema.Types.Mixed
  district: String  # Only required for dups matching
  domain: String
  fqdn: String
  url: String
  transactions:
    type: [Schema.Types.ObjectId]
    default: [Schema.Types.Mixed]
  online: Boolean
  overwritten: Boolean
  lastCrawled: Date
  lastPipelined: Date
  lastOffline: Date
},  { j: 1, w: 1, wtimeout: 10000 }

# Do not index the title as btree (MongoDB will complain about key being too large!)

RealEstate.index {url: 1}, {unique: true, dropDups: true}
RealEstate.index {'props.noCommission': 1}
RealEstate.index {district: 1}
RealEstate.index {extId: 1}, {unique: true, dropDups: true}
RealEstate.index {'meta.dups': 1, extId: 1}
# For stats and findStuffToRecrawl
RealEstate.index {lastCrawled: 1, online: 1, fqdn: 1}
RealEstate.index {online: 1}
RealEstate.index {lastOffline: 1}
# For Webmaster Tools
RealEstate.index {fqdn: 1, online: 1}
# Dedup indices: Optimized for prefix searches. Everything should have an index and an extId, likely has rooms
RealEstate.index { district: 1, extId: 1, 'props.type': 1, 'props.rentalFee': 1, 'props.livingSpace': 1, 'props.rooms': 1 }
RealEstate.index { district: 1, extId: 1, 'props.type': 1, 'props.purchasePrice': 1, 'props.livingSpace': 1, 'props.rooms': 1 }
# TODO: Reactivate once MongoDB bug is gone
#RealEstate.index {'props.title': 'text', extId: 'text', 'geo.topo.name': 'text', 'geo.topo.id': 'text', url: 'text'}, { default_language: 'de' }
RealEstate.index { 'geo.topo.id': 1, 'geo.topo.name': 1, extId: 1}

module.exports = RealEstate