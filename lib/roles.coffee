{conn, Schema} = require './_mongo'

q = require "q"
_ = require "lodash"

Roles = Schema
  _id: String
  name: String

Roles.index {_id: 1}, {unique: true, dropDups: true}

module.exports = Roles

