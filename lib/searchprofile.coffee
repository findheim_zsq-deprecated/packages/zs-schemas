{conn, Schema} = require './_mongo'

q = require "q"
_ = require "lodash"

SearchProfile = Schema
  _id: String
  name: String
  isDefault: Boolean
  createdAt: Date
  country: String
  searchQuery:
    type: [String]
    rentalFee: [Number]
    purchasePrice: [Number]
    livingSpace: [Number]
    annualLeaseFee: [Number]
    plotArea: [Number]
    rooms: [Number]
    geoEntities: [String]
    createdAfter: String
    bbox: Schema.Types.Mixed
    geoFields: [String]
    features: Object
    features_filter: Object
    sort: [
      _id: false
      sortBy: String
      order: String
    ]
  tags: [Schema.Types.Mixed]
  lists:
    faved: [
      _id: false
      extId: String
      dateFaved: Date
    ]
    trashed: [
      _id: false
      extId: String
      dateTrashed: Date
    ]
  categories:
    faved: [String]
    trashed: [String]
    read: [String]
    trefferupdate: [
      _id: false
      extId: String
      exact: Boolean
    ]
  push:
    enabled: Boolean      # old < 2.0.0 format
    deviceids: [String]   # old < 2.0.0 format
    deviceIds: Object     # new format: hash deviceid -> enabled
    isLoggedIn: Object    # for recovering push notification state after log out  
  zionic:
    geoLocation: String   # last current location
    basisProfile: Object  # boolean hash propname->isBasisProfile
    wizard: Object        # boolean hash q_id->isAnswered
  trefferupdate:
    active: Boolean
    onlyExact: Boolean
    lastSent: Date
    interval: Number
    stats: Schema.Types.Mixed
  migrated: Boolean
,
  typeKey: '$type'

SearchProfile.index {_id: 1}, {unique: true, dropDups: true}

## INSTANCE METHODS
SearchProfile.methods.getDeviceIds = ->
  @push?.deviceids or []

SearchProfile.methods.addDeviceIds = (deviceIds) ->
  unless deviceIds? and (_.isString(deviceIds) or _.isArray(deviceIds))
    return
  if _.isString(deviceIds)
    deviceIds = [deviceIds]

  unless @push?
    @push = {}
  unless @push.deviceids?
    @push.deviceids = []

  @push.deviceids = _.uniq @push.deviceids.concat deviceIds

SearchProfile.methods.removeDeviceIds = (deviceIds) ->
  unless deviceIds? and (_.isString(deviceIds) or _.isArray(deviceIds))
    return
  if _.isString(deviceIds)
    deviceIds = [deviceIds]

  unless @push?
    @push = {}
  unless @push.deviceids?
    @push.deviceids = []

  @push.deviceids = _.difference @push.deviceids, deviceIds


## STATICS

SearchProfile.statics.getDeviceIds = (profileId) ->
  d = q.defer()
  @findOne({"_id":profileId}).exec (err, profile) ->
    if err?
      d.reject new Error "Failed to get SearchProfile: #{err.message or err}"
    else if not profile?.push?.deviceids?
      d.reject new Error "Profile .push.deviceids missing"
    else if not _.isArray(profile.push.deviceids)
      d.reject new Error "Profile .push.deviceids is not an array, got #{JSON.stringify profile.push.deviceids}"
    else
      d.resolve profile.getDeviceIds()

  d.promise

SearchProfile.statics.addDeviceIds = (profileId, deviceIds) ->
  d = q.defer()
  @findOne({"_id":profileId}).exec (err, profile) ->
    if err?
      d.reject new Error "Failed to get SearchProfile: #{err.message or err}"
    else
      profile.addDeviceIds deviceIds
      profile.save (err)->
        if err?
          d.reject new Error "Failed to add deviceid: #{err.message or err}"
        else
          d.resolve()
  d.promise

SearchProfile.statics.removeDeviceIds = (profileId, deviceIds) ->
  d = q.defer()
  @findOne({"_id":profileId}).exec (err, profile) ->
    if err?
      d.reject new Error "Failed to get SearchProfile: #{err.message or err}"
    else
      profile.removeDeviceIds deviceIds
      profile.save (err)->
        if err?
          d.reject new Error "Failed to remove deviceid: #{err.message or err}"
        else
          d.resolve()
  d.promise

# convenience method. eg:
# getDeviceIds(profileId) == execMethod(profileId,'getDeviceIds')
# addDeviceId(profileId, deviceId) == execMethod(profileId,'addDeviceIds',deviceId,true)
SearchProfile.statics.execMethod = (profileId, methodName, arg, doSave) ->
  d = q.defer()
  @findOne({"_id":profileId}).exec (err, profile) ->
    if err?
      d.reject new Error "Failed to get SearchProfile: #{err.message or err}"
    else
      if doSave
        profile[methodName](arg)
        profile.save (err)->
          if err?
            d.reject new Error "Failed to save SearchProfile: #{err.message or err}"
          else
            d.resolve()
      else
        d.resolve profile[methodName](arg)

  d.promise

module.exports = SearchProfile

#{
#"_id" : "zQAZs6B7tvpGakPCN",
#"categories" : {
#  "faved" : [
#    "derstandard.at7235252"
#  ],
#  "read" : [
#    "www.immobilien.net1654-1561",
#    "www.wohnnet.at149745029",
#    "www.wohnnet.at148642806",
#    "www.wohnnet.at149820541",
#    "derstandard.at6895458",
#    "www.finden.at7243962",
#    "www.wohnnet.at149739871",
#    "www.immowelt.at32348118",
#    "derstandard.at7235252",
#    "www.immobilien.net2498-304990",
#    "www.immobilien.net1910-46",
#    "www.immobilien.net1049-20442",
#    "derstandard.at7230982",
#    "www.immobilien.net6000-161",
#    "www.immobilien.net2095-80139",
#    "www.finden.at7144579",
#    "www.wohnnet.at148632028",
#    "www.immobilien.net2700-3119",
#    "www.immoversum.com650549",
#    "www.immobilien.net20147-5319",
#    "www.immobilien.net6146-1275",
#    "www.immoversum.com652803",
#    "www.immobilien.net7350-14808",
#    "www.immobilien.net20110-1163",
#    "www.immobilienring.at20951",
#    "www.finden.at7248306",
#    "www.wohnnet.at149911385",
#    "www.finden.at7227083",
#    "www.immoversum.com647122",
#    "www.wohnnet.at149214545",
#    "www.finden.at7225985",
#    "www.finden.at7245897",
#    "www.immobilien.net1231-5061",
#    "www.finden.at7192251",
#    "www.finden.at7274152",
#    "www.finden.at7249205",
#    "www.immobilien.net6935-1801",
#    "www.finden.at7232969",
#    "www.immoversum.com662667",
#    "www.finden.at7270147",
#    "immoads.oe24.at959399",
#    "derstandard.at7217604",
#    "www.finden.at7257648",
#    "www.a-immobilienmarkt.com94024-10-2013",
#    "www.finden.at7282728",
#    "derstandard.at7282378",
#    "www.finden.at7253398",
#    "www.finden.at7303951",
#    "www.immodirekt.at1720954",
#    "www.wohnnet.at149580666",
#    "www.immobilien.net6670-4218",
#    "www.immoversum.com669810",
#    "www.finden.at7277549",
#    "www.wohnnet.at149187111",
#    "www.immoversum.com673053",
#    "www.findmyhome.at1397642",
#    "www.wohnnet.at150585237",
#    "www.immobilien.net4768-3605",
#    "derstandard.at7341872",
#    "derstandard.at7344085"
#  ],
#  "trashed" : [
#    "www.immobilien.net1910-46",
#    "www.finden.at7235252",
#    "www.immobilien.net20324-3254"
#  ]
#},
#"isDefault" : true,
#"name" : "Mein Suchprofil",
#"tags" : [
#  {
#    "type" : "geoentity",
#    "name" : "Leopoldstadt, Wien",
#    "key" : "355787884",
#    "_id" : "NbtifAhJwQML8YPE3"
#  },
#  {
#    "type" : "option",
#    "key" : "oldBuilding",
#    "importance" : 0,
#    "name" : "Altbau",
#    "_id" : "f8JrzCp7CFDrgr9G9"
#  },
#  {
#    "type" : "option",
#    "key" : "balcony",
#    "importance" : 0,
#    "name" : "Balkon",
#    "_id" : "2ZkYmv4gXSeYYToZw"
#  },
#  {
#    "type" : "option",
#    "key" : "rentalUnlimited",
#    "importance" : 1,
#    "name" : "unbefristet",
#    "_id" : "69SJjjwfMY7qSQiWf"
#  },
#  {
#    "type" : "price",
#    "key" : "purchasePrice",
#    "val" : [
#      12000,
#      163000
#    ],
#    "name" : "Kaufpreis",
#    "unit" : " €",
#    "_id" : "xnKd2JjjozcHfrArB"
#  },
#  {
#    "type" : "space",
#    "key" : "livingSpace",
#    "val" : [
#      10,
#      80
#    ],
#    "name" : "Wohnfläche",
#    "unit" : " m²",
#    "_id" : "df4Fxp6Di8ACbJv2o"
#  },
#  {
#    "type" : "space",
#    "key" : "rooms",
#    "val" : [
#      1,
#      4
#    ],
#    "name" : "Zimmer",
#    "unit" : "",
#    "_id" : "PHjFi86MRyhmNiPic"
#  },
#  {
#    "type" : "type",
#    "key" : "Apartment",
#    "name" : "Wohnung",
#    "_id" : "4iHSr6cB9cHRco7q4"
#  }
#]
#}