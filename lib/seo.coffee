{conn, Schema} = require './_mongo'

seo = Schema
  project: String
  path: String
  language: String
  title: String
  description: String
  keywords: String
  robots: String
, {collection: 'seo'}

module.exports = seo