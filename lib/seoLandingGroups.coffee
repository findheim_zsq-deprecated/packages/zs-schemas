{conn, Schema} = require './_mongo'

seoLandingGroups = Schema
  createdAt: Date
  name : String
  desc : String
  active: Boolean
  urlParam: String
  topoKey: String
  topoLabel: String
  ownType: String
  useType: String
  option: []
  price:
    from: String
    to: String
  rooms:
    from: String
    to: String
  livingSpace:
    from: String
    to: String

seoLandingGroups.index {_id: 1}, {unique: true, dropDups: true}

module.exports = seoLandingGroups