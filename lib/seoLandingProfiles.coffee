{conn, Schema} = require './_mongo'

q = require "q"
_ = require "lodash"

seoLandingProfiles = Schema
  _id: String
  key: String
  name: String
  primary: Boolean
  secondary: Boolean
  createdAt: Date
  country: String
  suffix: String
  prefix: String
  path: String
  keywords: [String]
  sort: Number
  exclude: [String]
  below: [String]
  hidden: Boolean
  searchQuery:
    useType: String
    ownType: String
    rentalFee: [Number]
    purchasePrice: [Number]
    livingSpace: [Number]
    annualLeaseFee: [Number]
    plotArea: [Number]
    rooms: [Number]
    geoEntities: [String]
    props: Object
,
  typeKey: '$type'

seoLandingProfiles.index {_id: 1}, {unique: true, dropDups: true}

seoLandingProfiles.statics.getPrimaryProfiles = (options) ->
  deferred = q.defer()

  # load primary profiles from mongo
  @find({"country": options.country, "primary": true, "secondary": false}).lean().exec (err, profiles) ->
    deferred.resolve _.sortBy profiles, 'sort'

  deferred.promise

seoLandingProfiles.statics.getSecondaryProfiles = (options) ->
  deferred = q.defer()
  
  # load primary profiles from mongo
  @find({"country": options.country, "primary": false, "secondary": true}).lean().exec (err, profiles) ->
    deferred.resolve _.sortBy profiles, 'sort'

  deferred.promise

seoLandingProfiles.statics.getMutations = (options) ->
  deferred = q.defer()

  mainProfile = {id: "main", name: "main", filter:{}, "sort":0, keywords:[]}

  primaryProfiles = []
  mainProfiles = [mainProfile]

  all = mainProfiles.concat([])

  _findPrimaries =  (secondary) ->
    primaries = []
  
    for profile in mainProfiles
      # has to match useType, ownType and primary id should not be in exclude list (any true if property is not present in secondary)
      primaries.push profile if _matchOrUndefined(profile, 'useType', secondary) and _matchOrUndefined(profile, 'ownType', secondary) and not(profile.name in (secondary.exclude ? []))

    return primaries

  _matchOrUndefined = (profile, key, secondaryProfile) -> (!secondaryProfile.searchQuery? or (secondaryProfile.searchQuery? and !secondaryProfile.searchQuery[key]?)) or (profile.searchQuery? and profile.searchQuery[key] is secondaryProfile.searchQuery[key])

  # fetch primary profiles from mongo
  @find({"country": options.country, "primary": true, "secondary": false}).lean().exec (err, profiles) ->
    primaryProfiles = _.sortBy profiles, 'sort'
    mainProfiles = mainProfiles.concat primaryProfiles
    all = all.concat primaryProfiles
  .then =>
    # fetch secondary profiles from mongo
    @find({"country": options.country, "primary": false, "secondary": true}).lean().exec (err, secondaries) ->
      _.forEach secondaries, (secondaryProfile) ->
        primaries = _findPrimaries secondaryProfile

        _.forEach primaries, (primary) ->
          keywordsCopy = primary.keywords

          clone = _.merge(_.cloneDeep(primary), secondaryProfile)
          clone.parent = primary.key
          clone.keywords = keywordsCopy.concat secondaryProfile.keywords

          all.push clone

      deferred.resolve all

  deferred.promise
  
module.exports = seoLandingProfiles