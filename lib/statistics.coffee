{conn, Schema} = require './_mongo'

statistics = Schema
  type: String
  timestamp: {type: Date, default: Date.now}
  data: Schema.Types.Mixed
, {collection: 'statistics'}

module.exports = statistics