{conn, Schema} = require './_mongo'

translations = Schema
  project: String
  created: {type: Date, default: Date.now}
  changed: {type: Date, default: Date.now}
  key: String
  default: String
  translations:
    en:
      text: String
      status: String
  location: [
    _id : false
    project: String
    file: String
    line: Number
    column: Number
  ]
  status: String
  parameters: [String]
, {collection: 'translations'}

translations.index {_id: 1}, {unique: true, dropDups: true}

module.exports = translations