{conn, Schema} = require './_mongo'

translationsV2 = Schema
  project: String
  country: String
  created: {type: Date, default: Date.now}
  changed: {type: Date, default: Date.now}
  key: String
  translations:
    de:
      text: String
      status: {type: String, enum: ["active","dirty"]}
    en:
      text: String
      status: {type: String, enum: ["active","dirty"]}
  locations: [
    _id : false
    project: String
    country: String
    file: String
    line: Number
    column: Number
    params: [String]
  ]
  status: {type: String, enum: ["active","dirty","unused"]}
, {collection: 'translationsV2'}

translationsV2.index {_id: 1}, {unique: true, dropDups: true}
translationsV2.index {project: 1}
translationsV2.index {country: 1}
translationsV2.index {key: 1}
translationsV2.index {status: 1}

module.exports = translationsV2