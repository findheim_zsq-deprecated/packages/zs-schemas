{conn, Schema} = require './_mongo'

User = Schema
  _id: String
  createdAt: Date
  createdLabel: String
  createdCountry: String
  createdUtm:
    _id: false
    source: String
    medium: String
    campaign: String
    term: String
    content: String
  emails: [
    _id : false
    address: String
    verified: Boolean
    unverifyReason: String
    unverifyDetails: String
    duplicate: Boolean
  ]
  isDefault : [Boolean]
  profile:
    _id : false
    name: String
    lastChangelogChecked: Number
    area: String
    lastLastLogin: Date
    lastLogin : Date
    lastLoginMobile : Date
    lastWeekly : Date
    online : Boolean
    points : Number
    searchprofiles : [String]
    sirname : String
    firstname: String
    survey :
      _id : false
      recommend :
        _id : false
        answer : String
        time : Date
      premium :
        _id : false
        answer : String
        time : Date
      sharing :
        _id : false
        answer : String
        time : Date
    vis :
      _id : false
      searchGuideBtn : Boolean
      searchGuideHidden : Boolean
      criteriasbox: Boolean
      family: Boolean
      newPipeline: Boolean
      searchv8: Boolean
      sparetime: Boolean
      zoomscore: Boolean
      zoomstats: Boolean
    nps:
      _id: false
      vote: Number
      date: Date
    inviteId: String
    invited: [String]
    invitedBy: String
    language: String
    weekly: Boolean
    weeklyUnsubscribeReason: String
    unsubscribed: Boolean
    nopwdRemindersSent: Number
    lastEmailVerificationReminder: Date
    nopwd: Boolean
    betaStatus: String
    personId: String
    contactData:
      firstName: String
      lastName: String
      email: String
      phone: String
      message: String
      terms: Boolean
    wizardData:
      shown: Date
      gender: String
      age: String
      livingSituation: String
      searchingSince: String
      movingIn: String
    doNotUseProfileData:
      $type: Boolean
      default: false
  roles : [String]
  services :
    _id : false
    facebook:
      _id : false
      accessToken : String
      expiresAt : Number
      id : String
      email : String
      name : String
      first_name : String
      last_name : String
      link : String
      username : String
      gender : String
      locale : String
    email :
      _id : false
      verificationTokens : [
        _id : false
        token : String
        address : String
        when : Date
      ]
    password :
      _id : false
      bcrypt: String
      reset:
        _id : false
        token: String
        email: String
        when: Date
        tokenType: String
      srp :
        _id : false
        identity : String
        salt : String
        verifier : String
    resume :
      _id : false
      loginTokens : [
        _id : false
        when : Date
        hashedToken : String
      ]
  stats :
    _id : false
    login: [
      _id: false
      date: Date
      type: String
    ]
    loggedInMobile : [
      _id : false
      date : Date
      logType : String
    ]
    trefferupdate:
      _id : false
      tries : Number
      sent : Number
      queued : Number
      rejected : Number
      rejectReason : [String]
      unsubscribeReason : String
  status :
    _id : false
    online: Boolean
    idle: Boolean
    lastLogin:
      _id : false
      date : Date
      ipAddr : String
      userAgent : String
  lists:
    faved: [
      _id: false
      extId: String
      date: Date
    ]
    trashed: [
      _id: false
      extId: String
      date: Date
    ]
    viewed: [ # every estate the user viewed
      _id: false
      extId: String
      date: Date
    ]
    # TODO check for references and remove lead
    lead: [ # every lead the user generates
      _id: false
      extId: String
      date: Date
      type:
        $type: String
        enum: ["premium","listing","external"]
    ]
  migrated: Boolean
,
  typeKey: '$type' #change typeKey, because type is used as a field name

User.index {_id: 1}, {unique: true, dropDups: true}
User.index {"profile.inviteId": 1}, {unique: true}

module.exports = User