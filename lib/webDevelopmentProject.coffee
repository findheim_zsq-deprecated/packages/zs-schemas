{conn, Schema} = require './_mongo'

webDevelopmentProject = Schema
  meta:
    _id: false
    created:
      _id: false
      date: Date
      by:
        _id: false
        id: Schema.Types.ObjectId
        name: String
    lastModified:
      _id: false
      date: Date
      by:
        _id: false
        id: Schema.Types.ObjectId
        name: String
  featured: Boolean
  published: Boolean
  country: String
  url: String
  uniqueUrl: String
  name: String
  websiteUrl: String
  specialWebsiteUrl: String
  contactEmail: String
  contact:
    name: String
    phone: String
    email: String
  totalUnits: Number
  availableUnits: Number
  priceRange:
    min: Number
    max: Number
  sizeRange:
    min: Number
    max: Number
  roomsRange:
    min: Number
    max: Number
  units: [{
    price: Number
    size: Number
    rooms: Number
  }]
  currentStatus:
    type: String
    # enum: ['unknown', 'finished', 'planning', 'building']
  features: [String]
  geoEntity: Schema.Types.Mixed
  googleGeoEntity: Schema.Types.Mixed
  descriptionText: String
  locationText: String
  metaTitle: String
  metaDescription: String
  finishedDate: Date
  finishedDateDisplay: String
  types:
    _id: false
    own: String
    view: String
    use: String
  developerId: Schema.Types.ObjectId
  manualData:
    name: String
    descriptionText: String
    locationText: String
    address: String
    specialWebsiteUrl: String
    priceRange:
      min: Number
      max: Number
    sizeRange:
      min: Number
      max: Number
    roomsRange:
      min: Number
      max: Number
    availableUnits: Number
    currentStatus:
      type: String
      # enum: ['unknown', 'finished', 'planning', 'building']
    contact:
      name: String
      phone: String
      email: String
    types:
      own: String
      view: String
      use: String
  crawledData:
    watchHash: String
    name: String
    descriptionText: String
    locationText: String
    address: String
    specialWebsiteUrl: String
    priceRange:
      min: Number
      max: Number
    sizeRange:
      min: Number
      max: Number
    roomsRange:
      min: Number
      max: Number
    availableUnits: Number
    currentStatus:
      type: String
      # enum: ['finished', 'planning', 'building', 'unknown']
    contact:
      name: String
      phone: String
      email: String
    types:
      own: String
      view: String
      use: String
    imagesCount: Number
    filesCount: Number

webDevelopmentProject.index {_id: 1}, {unique: true, dropDups: true}
webDevelopmentProject.index {url: 1}, {unique: true, dropDups: true}
webDevelopmentProject.index {uniqueUrl: 1}, {unique: true, dropDups: true}
webDevelopmentProject.index {country: 1}
webDevelopmentProject.index {published: 1}

module.exports = webDevelopmentProject