{conn, Schema} = require './_mongo'

webFiles = Schema
  category:
    type: String
    enum: ['file', 'image', 'floorplan', 'simpleImage', 'developerImage', 'developerLogo']
  sourceUrl: String
  title: String
  filename: String
  stored: Boolean
  order: Number
  parentObjectId: Schema.Types.ObjectId

webFiles.index {_id: 1}, {unique: true, dropDups: true}
webFiles.index {filename: 1}, {unique: true, dropDups: true}
webFiles.index {parentObjectId: 1}

module.exports = webFiles