{conn, Schema} = require './_mongo'

webPropertyDeveloper = Schema
  meta:
    _id: false
    created:
      _id: false
      date: Date
      by:
        _id: false
        id: Schema.Types.ObjectId
        name: String
    lastModified:
      _id: false
      date: Date
      by:
        _id: false
        id: Schema.Types.ObjectId
        name: String
  featured: Boolean
  published: Boolean
  country: String
  url: String
  websiteUrl: String
  contactEmail: String
  name: String
  metaTitle: String
  metaDescription: String
  descriptionText: String

webPropertyDeveloper.index {_id: 1}, {unique: true, dropDups: true}
webPropertyDeveloper.index {url: 1}, {unique: true, dropDups: true}
webPropertyDeveloper.index {country: 1}
webPropertyDeveloper.index {published: 1}

module.exports = webPropertyDeveloper