{conn, Schema} = require './_mongo'

webTopicPage = Schema
  meta:
    _id: false
    created:
      _id: false
      date: Date
      by:
        _id: false
        id: Schema.Types.ObjectId
        name: String
    lastModified:
      _id: false
      date: Date
      by:
        _id: false
        id: Schema.Types.ObjectId
        name: String
    queryHash: String
    textQuery: [String]

  published: Boolean
  country: String
  url: String
  metaTitle: String
  metaDescription: String 
  headline: String
  content: String

  query:
    _id: false
    geoEntities: [String]
    type: [String]
    rentalFee: [Number]
    purchasePrice: [Number]
    livingSpace: [Number]
    plotArea: [Number]
    rooms: [Number]
    features_filter: Object
    agentIds: [String]
    sort: [
      _id: false
      sortBy: String
      order: String
    ]
    isOpenimmo: Boolean
    isPremium: Boolean
    isFeatured: Boolean
,
  typeKey: '$type' #change typeKey, because type is used as a field name

webTopicPage.index {_id: 1}, {unique: true, dropDups: true}
webTopicPage.index {url: 1}, {unique: true, dropDups: true}
webTopicPage.index {country: 1}
webTopicPage.index {published: 1}
webTopicPage.index {metaTitle: 1}
webTopicPage.index {metaDescription: 1}
webTopicPage.index {headline: 1}
webTopicPage.index {content: 1}
webTopicPage.index {"meta.queryHash": 1}
webTopicPage.index {"meta.textQuery": 1}

module.exports = webTopicPage