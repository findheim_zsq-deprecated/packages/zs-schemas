{conn, Schema} = require './_mongo'
#bcrypt = require "bcrypt"

webmasterSchema = Schema
  email: { type: String, unique: true }
  validEmail: Boolean
  password: String
  immohosts: [String] # Watched (not necessarily owned/verified)
  admin: Boolean

webmasterSchema.index {email: 1}, {unique: true, dropDups: true}
webmasterSchema.index {immohosts: 1}

# Lowercase E-Mail
webmasterSchema.pre 'save', (next) ->
  webmaster = this
  webmaster.email.toLowerCase()
  next()

# Hash password, if it changed
webmasterSchema.pre 'save', (next) ->
  webmaster = this
  if not webmaster.isModified('password')
    return next()

  if webmaster.password.length < 5
    return next new Error("Password to short")

  bcrypt.genSalt (error, salt) ->
    if error
      return next error

    bcrypt.hash webmaster.password, salt, (error, hash) ->
      if error
        return next error
      webmaster.password = hash
      next()

# Check whether the password is correct
webmasterSchema.methods.comparePassword = (enteredPassword, cb) ->
  bcrypt.compare enteredPassword, this.password, (error, isCorrect) ->
    if error
      return cb error
    cb null, isCorrect

webmasterSchema.statics.findByEmail = (email, cb) ->
  this.findOne {email}, cb

module.exports = webmasterSchema