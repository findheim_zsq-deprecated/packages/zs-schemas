{conn, Schema} = require './_mongo'

XTR_Item = Schema
    estate_id: 	Schema.Types.ObjectId
    url: 		String
    props: 		Schema.Types.Mixed

# added by Imy - stores annotations the annotation versions sent to the extractor from the Golden Standard Corpus (stored in annotestate)
XTRannotEstate = Schema
  timestamp: 	String
  xtr_items: [XTR_Item]
  
XTRannotEstate.index {timestamp: 1}, {unique: true, dropDups: true}

module.exports = XTRannotEstate