
assert  = require "assert"
_       = require "lodash"

describe.only "immoHost", ->

  Immohost = null
  before ->
    Immohost = require("../").model "Immohost"

  it "regex", (done) ->
    Immohost.findOne({fqdn:"www.immonet.at"}).lean().exec (err, res) ->
      assert _.isRegExp res.config.follow
      done()

  it "works (method)", (done) ->
    Immohost
    .findOne {"config.justimmo": true}
    .exec (err, immoHost) ->
      if err
        done err
      else
        immoHost.justImmorize()
        assert immoHost.config.check.length > 0
        assert _.isRegExp immoHost.config.regex
        assert _.isRegExp immoHost.config.follow
        done()

   it "works (static)", (done) ->
    Immohost
    .findOne {"config.justimmo": true}
    .exec (err, immoHost) ->
      if err
        done err
      else
        Immohost.justImmorize immoHost
        assert immoHost.config.check.length > 0
        assert _.isRegExp immoHost.config.regex
        assert _.isRegExp immoHost.config.follow
        done()
