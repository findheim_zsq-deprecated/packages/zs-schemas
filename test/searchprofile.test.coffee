assert = require "assert"
SearchProfile = require "../lib/searchprofile"


# einfacher test ob schema eh geht
describe 'SearchProfile', ->
  before ->
    mongoose    = require 'mongoose'

    mongoose.connect 'mongodb://piffie:immo2020@mongodb0.zoomsquare.com,mongodb1.zoomsquare.com,mongodb2.zoomsquare.com/admin?replicaSet=zoomsquare&readPreference=secondaryPreferred'

  describe 'Connection', ->
    it 'should connect, and schema should not throw an error', (done) ->
      SearchProfile.findOne().where('_id').equals("zQAZs6B7tvpGakPCN").lean().exec (error, user) ->
        assert.ifError(error, "error returned")
        assert.equal(user._id, "zQAZs6B7tvpGakPCN", "incorrect _id retrieved")
        done()

    it 'should not stumble over old searchprofiles (messy, laden ones)', (done) ->
      SearchProfile.findOne().where('_id').equals("XXGDXywDMZTPr2ngi").lean().exec (error, user) ->
        assert.ifError(error, "error returned")
        assert.equal(user._id, "XXGDXywDMZTPr2ngi", "incorrect _id retrieved")
        done()


