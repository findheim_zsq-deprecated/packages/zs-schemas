assert = require "assert"
User = require "../lib/user"

# einfacher test ob schema eh geht
describe 'User', ->
  before ->
    mongoose    = require 'mongoose'
    mongoose.connect 'mongodb://piffie:immo2020@mongodb0.zoomsquare.com,mongodb1.zoomsquare.com,mongodb2.zoomsquare.com/admin?replicaSet=zoomsquare&readPreference=secondaryPreferred'

  describe 'Connection', ->
    it 'should connect, and schema should not throw an error', (done) ->
      User.findOne().where('_id').equals("j6bShAHcxGASGiLv3").lean().exec (error, user) ->
        assert.ifError(error, "error returned")
        assert.equal(user._id, "j6bShAHcxGASGiLv3", "incorrect _id retrieved")
        done()


